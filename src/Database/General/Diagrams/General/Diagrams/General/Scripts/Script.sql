

--drop table if exists reimbursement ;
--drop table if exists employee;



create table reimbursements (
	invoice_id serial primary key,
	first_name varchar(30), 
	last_name varchar(30),
	title varchar(30),
	total numeric (8,2),
	ID integer references  employee,
	is_approved boolean
)

insert into employee (first_name,last_name,title ,managerID) values (001,'Sally','Jenkins','worker',200);
insert into employee (first_name,last_name,email,managerID) values (002, 'Lola','Smith','worker', 200);

insert into reimbursements  (invoice_id, first_name, last_name, title, total, ID, is_approved) values (100,'John', 'Neo', 'Food', 12.00, 001, true);
insert into reimbursements  (invoice_id, first_name, last_name, title, total, ID, is_approved) values (101,'Benni', 'Nguyen', 'Valet', 18.00, 002, true);

select * from employee;
select * from reimbursements;
