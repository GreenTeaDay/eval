package daos;

import models.Items;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ItemsDaoImpTest {

    @Test
    void testGetAllItems() {
        List<Items> items = ItemsDao.getAllItems();
        assertAll(()->{
            assertNotNull(items);
            assertTrue(items.size()>0);
        });
    }


}