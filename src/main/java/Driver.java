import daos.ItemsDao;
import daos.ItemsDaoImp;
import models.Items;

import java.util.List;

public class Driver {

    public static void main(String[] args) {
        ItemsDao itemsDao = new ItemsDaoImp();
        List<Items> AllItems = ItemsDao.getAllItems();
        System.out.println(AllItems);
    }
};
