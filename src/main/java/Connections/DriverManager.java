package Connections;

import java.sql.Connection;
import java.sql.SQLException;

public class DriverManager {

    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String connectionString = "jdbc:postgresql://Inventory.cerxofpxvsnu.us-east-1.rds.amazonaws."+"com:5432/Inventory";

        String username = System.getenv("DB_USER");
        String password = System.getenv("DB_PASS");

        return java.sql.DriverManager.getConnection(connectionString, username, password);
    }
}
