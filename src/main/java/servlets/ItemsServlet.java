package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import daos.ItemsDao;
import daos.ItemsDaoImp;
import models.Items;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ItemsServlet extends HttpServlet {

    private ItemsDao itemsDao = new ItemsDaoImp();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Items> itemsList = ItemsDao.getAllItems();

        String taskJSON = objectMapper.writeValueAsString(itemsList);

        resp.setHeader("Content-Type", "application/json");

        try(PrintWriter pw = resp.getWriter()){
            pw.write(taskJSON);
        }
    }

}
