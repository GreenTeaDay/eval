package daos;

import models.Items;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.sql.DriverManager.getConnection;

public class ItemsDaoImp implements ItemsDao {

    public Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            conn = null;
        }
        return conn;
    }
    @Override
    public List<Items> getAllItems() {
        PreparedStatement stmt = null;
        Connection conn = getConnection();

        ArrayList<Items> AllItems = new ArrayList<Items>();
        final String SQL = "select * Inventory";

        try {
            if (conn != null) {
                stmt = conn.prepareStatement(SQL);
                ResultSet results = stmt.executeQuery();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return AllItems;
    }



}